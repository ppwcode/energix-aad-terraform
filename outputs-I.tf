output "I-developer_users_new" {
  value = [for u in azuread_user.new_developer_azuread_users : u.user_principal_name]
}

output "I-developer_users_existing" {
  value = [for u in data.azuread_user.existing_developer_azuread_users : u.user_principal_name]
}

output "I-ci_users_new" {
  value = [for u in azuread_user.new_ci_azuread_users : u.user_principal_name]
}
