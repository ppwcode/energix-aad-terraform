# subscription referenced by its alias
data "azurerm_subscription" "energix-sub" {}

# storage account that contains all terraform state
data "azurerm_storage_account" "tfstateenergix" {
  name                = "tfstateenergix"
  resource_group_name = "tfstate"
}

# developers need read-write terraform state (Azure level)
resource "azurerm_role_assignment" "developers-terraformstate-role" {
  scope                = data.azurerm_storage_account.tfstateenergix.id
  role_definition_name = "Storage Blob Data Owner"
  principal_id         = azuread_group.energix_developers_group.id
}

# developers need to be able to assign permissions (Azure level)
resource "azurerm_role_assignment" "developers-subscription-owner" {
  scope                = data.azurerm_subscription.energix-sub.id
  role_definition_name = "Owner"
  principal_id         = azuread_group.energix_developers_group.id
}

# the User Administrator role (Active Directory level)
resource "azuread_directory_role" "user-administrator" {
  display_name = "User administrator"
}

# developers need the User Administrator role (Active Directory level)
resource "azuread_directory_role_member" "user-administrator" {
  for_each         = toset([for o in local.all_developer_azuread_users : o.object_id])
  role_object_id   = azuread_directory_role.user-administrator.object_id
  member_object_id = each.value
}

/* this would be cleaner, but only supported on AD
# assign role to energix group
# only possible on AAD Premium
resource "azuread_directory_role_member" "user-administrator" {
  role_object_id   = azuread_directory_role.role-user-administrator.object_id
  member_object_id = azuread_group.energix_group.id
}
*/
