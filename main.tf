# Configure Terraform
terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 2.15.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.98.0"
    }
  }
}

# Configure the Azure Active Directory Provider
provider "azuread" {
  # PeopleWare's tenant ID
  tenant_id = local.tenant_id
}

# Configure the Azure provider (terraform state)
provider "azurerm" {
  features {}

  storage_use_azuread = true
  subscription_id     = local.subscription_id
}

# Developer user creation in a loop
resource "azuread_user" "new_developer_azuread_users" {
  for_each = { for user in local.developers_to_create : join(" ", compact([
    user.user_principal_name,
    user.display_name,
  user.password])) => user }
  user_principal_name = each.value.user_principal_name
  display_name        = each.value.display_name
  password            = each.value.password
}

# CI user creation in a loop
resource "azuread_user" "new_ci_azuread_users" {
  for_each = { for user in local.ci_users_to_create : join(" ", compact([
    user.user_principal_name,
    user.display_name,
  user.password])) => user }
  user_principal_name = each.value.user_principal_name
  display_name        = each.value.display_name
  password            = each.value.password
}

locals {
  # all developer users: existing and new
  all_developer_azuread_users = merge(data.azuread_user.existing_developer_azuread_users, azuread_user.new_developer_azuread_users)
}

# Create developers group
resource "azuread_group" "energix_developers_group" {
  display_name       = local.developer_group
  assignable_to_role = false # only supported on AAD Premium
  security_enabled   = true
  members            = [for o in local.all_developer_azuread_users : o.object_id]
}

# Create ci users group
resource "azuread_group" "energix_ci_group" {
  display_name       = local.ci_group
  assignable_to_role = false # only supported on AAD Premium
  security_enabled   = true
  members            = [for o in azuread_user.new_ci_azuread_users : o.object_id]
}

/* The following section is commented since the tenant is not licensed for this feature.

resource "azuread_conditional_access_policy" "mfa_access_policy" {
  display_name = "MFA Access Policy"
  state        = "enabled"

  conditions {
    client_app_types    = ["all"]
    sign_in_risk_levels = ["medium"]
    user_risk_levels    = ["medium"]

    applications {
      included_applications = ["All"]
    }

    locations {
      included_locations = ["All"]
    }

    platforms {
      included_platforms = ["all"]
    }

    users {
      included_groups = [azuread_group.energix_group.object_id]
    }
  }

  grant_controls {
    operator          = "AND"
    built_in_controls = ["mfa"]
  }
}
*/