// Create alias for existing Subscription
// see https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subscription
resource "azurerm_subscription" "energix-sub" {
  alias             = "energix-sub"
  subscription_name = "Visual Studio Enterprise Subscription – MPN"
  subscription_id   = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
}
