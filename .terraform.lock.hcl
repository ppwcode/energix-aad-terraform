# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/azuread" {
  version     = "2.15.0"
  constraints = "~> 2.15.0"
  hashes = [
    "h1:9VXImUw/N5trbHEQiE2jaRidbN0uMx7K1xNwA4J5rfk=",
    "h1:GoxH9byeIAXxW1dkLeMkVBeRpBkLAQU3u9iBhG27OhQ=",
    "zh:2fde02166f973f14c176aac01346bcce5aaa6fac34e47c83ab5a5bb2da4e554a",
    "zh:39f955acc2ae67c2f939cc949e9026b3f2ff04da1ef6f12a080f588a356de2d9",
    "zh:40efd3c358046788940560049a22588fb280e4720f77a861148d39e1855f357c",
    "zh:6a51776492efb85e5a792e082bceb2fc953ffca8e3c7b9f18b0702dc3b64b704",
    "zh:78fc976f020e7859141476fd930b48bc5701c0572db15dda38deb00f6f7698f0",
    "zh:79528395629153a84d45a14ecc525115ad17e7d9caab8d4561a5060658aeb8ee",
    "zh:cad982edfed3602eb85f3fd39a7783b210fa2786d53e14a0d454c0c2adc57d84",
    "zh:e39b7de43f5933e0e9d7eaeb89b5a015880dfe51697cd985ae31198cf08a5c75",
    "zh:ecd856ae46e22518c08c5db862844a8ed9052599c099e950e92b02352495c2c9",
    "zh:f8a5f15fc6f9d0b1eaeb9bdae2e506e1fc16f18103a314b0acbb3fb62bf86345",
    "zh:f984c2024599881e68d234e93951c3a54e849e28e15591f5659e83e480c41beb",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version     = "2.98.0"
  constraints = "2.98.0"
  hashes = [
    "h1:8Sg08lYcJC12Y8EH5oFfgBhIR9OhZFKF633NjOMjilY=",
    "h1:AjhziX4Y2XyMvXl+VMnq9gDt/snuE4F3STxEtXdx9kE=",
    "zh:025f656a6d3ecc30f7cc2279bc41969789987b405e3fa8a7c1eb5f74e3ee1140",
    "zh:23c54b330678a16378156193d709bbddce3ba76ee827fd65fb751ce90790af9e",
    "zh:2d28d359ce6881918bd6c03701f6ec4fd90215abfce9b863cfd3172e28c1acb3",
    "zh:31df88584d39cf876fa45ff6de92e67e03814a0985d34c7671bd6989cda22af8",
    "zh:36019109790b9a905770355e5bbb57b291a9689a8b9beac5751dcbdb1282d035",
    "zh:5fb4a277331c459db9e1b150d79b7c7157a176ceca871195e81225e949141b72",
    "zh:7ec304afa1b60dc84257a54cea68e97f85df3feb405d25a9226a4f593ed00744",
    "zh:bac469f104b8ad2c8b5ddc88ddae3b0bc27ae5f9c2ccf03f14a001a5c3ed6ae1",
    "zh:d860b0ec60a978fe3f08d695326e9051a61cd3f60786fc618a61fbdb5d6a4f15",
    "zh:ebcb2911ee27587f63df7eff3836c9a206181a931357c6b9a380124be4241597",
    "zh:f37fae57bf7d05c30fda6e5414ad5a4aad1b34d41a5f2465a864736f92ded1ac",
  ]
}
