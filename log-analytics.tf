

resource "azurerm_log_analytics_workspace" "energix-log-analytics" {
  name                = "energix-log-analytics"
  location            = azurerm_resource_group.energix.location
  resource_group_name = azurerm_resource_group.energix.name
  sku                 = "PerGB2018"
  retention_in_days   = 30
}


