Notes regarding the creation of users in Active Directory
=========================================================

We try to do as much as possible for the configuration using Terraform. However,
some things are not possible yet with the current terraform provider.


Role assignment regarding the use of an alias on the subscription
-----------------------------------------------------------------

Everyone who must be able to apply a Terraform configuration, must have the role
assignments to be able to read the subscription name aliases.  It is currently
not possible to assign these roles because this is based on a scope across
subscriptions and the current provider expects a subscription specific scope.

The following commands can be executed manually.
Note that it should be possible to execute similar commands using the azure cli
tools, but that tool crashed for me, so I used the PowerShell module.
The following code can be used.

```powershell
Install-Module -Name Az -Scope CurrentUser -Repository PSGallery -Force # install PowerShell module
Connect-AzAccount                                                       # alternative for 'az login'
Set-AzContext -Subscription "098ad191-f2fd-4240-9a7c-e269ab8394c0"      # use correct subscription

# assign the new role on the microsoft subscriptions level
New-AzRoleAssignment -SignInName "energix_turbo@peoplewarenv.onmicrosoft.com" -RoleDefinitionName "Reader" -Scope "/providers/Microsoft.Subscription"

# or in bulk
$accounts = ("energix_user_1","energix_user_2","energix_user_3")
$accounts || %{ New-AzRoleAssignment -SignInName "$($_)@peoplewarenv.onmicrosoft.com" -RoleDefinitionName "Reader" -Scope "/providers/Microsoft.Subscription" }
```

Do note that it is only possible to apply these when the account that is used
for this has elevated access.  See [here](https://docs.microsoft.com/en-us/azure/role-based-access-control/elevate-access-global-admin#azure-portal).



Updating the configuration with Terraform
=========================================


Prerequisites
-------------

Terraform installed.

Read on how to login and get subscription id and tenant id:
```
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/azure_cli
```

Notes
-------------
MFA cannot be enforced due to tenant's license limitations.

Execution
---------

Update tenant id value if meant to be different than PeopleWare's on <main.tf>
```
# Configure the Azure Active Directory Provider
provider "azuread" {
    # PeopleWare's tenant ID
    tenant_id = "6bf21748-a694-4bd1-b1c4-6563b75bde06"
}
```

Run Powershell and change directory to this one

```shell
terraform init      # to download the provider
terraform fmt       # to format source files
terraform validate  # to check that the structure is OK
terraform apply     # to run
   # Type "yes" when prompted. 
   # Check your created infrastructure at the tenant's AAD, on Users and Groups
```


Optional:

```shell
terraform destroy # to remove all the created infrastructure
```